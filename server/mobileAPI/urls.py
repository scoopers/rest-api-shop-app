from django.urls import path, include
from rest_framework import routers
from . import views

# router = routers.DefaultRouter()
# router.register(r'basket', views.CartViewSet, basename='cart')

urlpatterns = [
    path('', views.UserListView.as_view()),
    #path('',include(router.urls)),
    path('account/', views.account),
    path('cart/', views.cart),
    path('cart/add/', views.add_to_cart),
    path('cart/remove/', views.remove_from_cart),
    path('auth/', include('rest_auth.urls')),
    path('auth/registration/', include('rest_auth.registration.urls')),
    path('list_products/', views.ProductsListView.as_view())
    # for development and protoyping only
]
