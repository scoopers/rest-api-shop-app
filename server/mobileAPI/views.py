from django.shortcuts import render, get_object_or_404
#from django.http import Http404
from rest_framework.views import APIView
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from .models import Product, ProductInCart
from .serializers import ProductSerializer, UserSerializer, CartSerializer
from django.contrib.auth.models import User
import json
import mimetypes

# figure out way to use "manytomany through" to add to quantity
# maybe replace cart with user so a user has a product in cart many to many field with products
# and the product in cart quantity is updated every time an exisiting item is added and a new one created
# every time a new item is added

# @api_view(["GET"])
# def list(request):
#     product = Product.objects.all()
#     serializer = ProductSerializer(product, many=True)
#     return JsonResponse(serializer.data, status=200, safe=False)

class ProductsListView(generics.ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

class UserListView(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class CartViewSet(viewsets.ViewSet):
    permission_classes = (IsAuthenticated,)
    def retrieve(self, request, pk=None):
        queryset = User.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        serializer = CartSerializer(user.customer.cart)
        return Response(serializer.data)

@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def account(request):
    try:
        user = request.user
    except:
        return Response('User does not exist.', status=404)
    serializer = UserSerializer(user)
    return Response(serializer.data)


@api_view(["GET"])
@permission_classes((IsAuthenticated,))
def cart(request):
    try:
        user = request.user
    except:
        return Response('User does not exist.', status=404)
    serializer = CartSerializer(user.customer.cart)
    return Response(serializer.data)


@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def add_to_cart(request):
    body = json.loads(request.body.decode("utf-8"))
    pid = int(body['pid'])
    user = request.user
    product = Product.objects.get(pk=pid)
    if product is None:
        return Response("Could not add to cart.", status=503)
    # user.customer.cart.products.add(product)
    product_in_cart = ProductInCart.objects.create(product=product, cart=user.customer.cart, quantity=1)
    product_in_cart.product.stock -= 1
    product_in_cart.product.save()
    product_in_cart.cart.total += product_in_cart.product.price
    product_in_cart.cart.save()
    serializer = ProductSerializer(product)
    return Response(serializer.data, status=201)

@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def remove_from_cart(request):
    body = json.loads(request.body.decode("utf-8"))
    pid = int(body['pid'])
    user = request.user
    product = Product.objects.get(pk=pid)
    # try:
    #     user.customer.cart.products.get(pk=pid)
    # except:
    #     return Response("Product is not in cart.", status=503)
    # user.customer.cart.products.remove(product)
    in_cart = ProductInCart.objects.filter(cart=user.customer.cart,product=product)
    quantity = len(in_cart)
    if quantity==0:
        return Response("Product is not in cart.", status=503)
    in_cart.delete()
    product.stock += quantity
    product.save()
    user.customer.cart.total -= product.price * quantity
    user.customer.cart.save()
    serializer = ProductSerializer(product)
    return Response(serializer.data, status=201)
