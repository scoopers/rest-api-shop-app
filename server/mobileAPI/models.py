from django.db import models
from django.contrib.auth.models import User

class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    cart = models.OneToOneField('Cart', on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username

class Category(models.Model):
    name = models.CharField(max_length=64)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name

class Product(models.Model):
    category = models.ForeignKey('Category', on_delete=models.CASCADE)
    name = models.CharField(max_length=64)
    description = models.TextField(max_length=256)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    stock = models.IntegerField()
    uploaded_at = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to='images/', default='images/none.jpg')

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name

class Cart(models.Model):
    products = models.ManyToManyField(Product, blank=True, through='ProductInCart')
    total = models.DecimalField(max_digits=50, decimal_places=2, default=0.0)

class ProductInCart(models.Model):
    product = models.ForeignKey('Product', on_delete=models.SET_NULL, null=True)
    cart = models.ForeignKey('Cart', on_delete=models.SET_NULL, null=True)
    quantity = models.IntegerField()
