from .models import Product, Category, Cart
from rest_framework import serializers
from django.contrib.auth.models import User

class ProductSerializer(serializers.ModelSerializer):
    category_name = serializers.CharField(source='category.name')

    class Meta:
        model = Product
        fields = ('id','category_name','name','description','price','stock','image')

class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ('name')

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','username','email', 'first_name', 'last_name')

class CartSerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True)

    class Meta:
        model = Cart
        fields = ('products', 'total')
