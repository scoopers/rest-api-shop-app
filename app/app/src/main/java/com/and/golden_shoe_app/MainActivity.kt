package com.and.golden_shoe_app

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.*
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject

class MainActivity : AppCompatActivity() {
    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val listurl = "http://10.0.2.2:8000/api/p/list_products/"

        val all_products = arrayListOf<Product>()

        val FILE_KEY = "GoldenShoe"
        val sharedPref = this.getSharedPreferences(FILE_KEY, Context.MODE_PRIVATE)
        val KEY_TOKEN = "Token"
        with(sharedPref.edit()) {
            putString(KEY_TOKEN, "")
            commit()
        }

        val queue = RequestSingleton.getInstance(this.applicationContext).requestQueue
        val request = StringRequest(Request.Method.GET, listurl, Response.Listener<String>{response ->
            Log.d("DEBUG", response.toString())
            val jsonStr = response.toString()
            val jsonArr = JSONArray(jsonStr)
            for(i in 0 until jsonArr.length()) {
                var jsonInner: JSONObject = jsonArr.getJSONObject(i)
                val id = jsonInner.get("id").toString().toInt()
                val category = jsonInner.get("category_name").toString()
                val stock = jsonInner.get("stock").toString().toInt()
                val description = jsonInner.get("description").toString()
                val name = jsonInner.get("name").toString()
                val photoUrl = jsonInner.get("image").toString()
                val price = jsonInner.get("price").toString().toDouble()
                all_products.add(Product(id, category, name, description, photoUrl, price, stock))
            }
            val gridLayoutManager = GridLayoutManager(this, 2)
            rv_products.layoutManager = gridLayoutManager
            rv_products.adapter = RecyclerAdapter(all_products)
        },
            Response.ErrorListener {Toast.makeText(this, "Cannot make request.", Toast.LENGTH_LONG).show()})

        RequestSingleton.getInstance(this.applicationContext).addToRequestQueue(request)

        b_account.setOnClickListener() {
            val url = "http://10.0.2.2:8000/api/p/account/"
            val payload = JSONObject()
            val accountReq = object: JsonObjectRequest(Request.Method.GET, url, payload, Response.Listener<JSONObject>{
                Log.d("DEBUG", it.toString())
                val intent = Intent(this, AccountPage::class.java)
                intent.putExtra("username",it.get("username").toString())
                intent.putExtra("email",it.get("email").toString())
                intent.putExtra("first_name",it.get("first_name").toString())
                intent.putExtra("last_name",it.get("last_name").toString())
                this.startActivity(intent)

            }, Response.ErrorListener { intent = Intent(this, LoginActivity::class.java); this.startActivity(intent) })  {
                override fun getHeaders(): MutableMap<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Authorization"] = "Token  ${sharedPref.getString(KEY_TOKEN, "")}"
                    return headers
                }
            }

            RequestSingleton.getInstance(this.applicationContext).addToRequestQueue(accountReq)

        }

        b_cart.setOnClickListener() {
            val url = "http://10.0.2.2:8000/api/p/cart/"
            val payload = JSONObject()
            val cartReq = object: JsonObjectRequest(Request.Method.GET, url, payload, Response.Listener<JSONObject>{
                val products = it.get("products")
                intent = Intent(this, CartPage::class.java)
                intent.putExtra("products", products.toString())
                intent.putExtra("total", it.get("total").toString())
                this.startActivity(intent)
            }, Response.ErrorListener { Toast.makeText(this, "Need to be logged in!", Toast.LENGTH_LONG).show() }) {
                override fun getHeaders(): MutableMap<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Authorization"] = "Token  ${sharedPref.getString(KEY_TOKEN, "")}"
                    return headers
                }
            }
            RequestSingleton.getInstance(this).addToRequestQueue(cartReq)
        }


    }


}
