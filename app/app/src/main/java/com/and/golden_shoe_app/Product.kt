package com.and.golden_shoe_app

data class Product(val id: Int, val category: String, val name: String, val description: String, val photoUrl: String, val price: Double, val stock: Int)