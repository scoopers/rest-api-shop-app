package com.and.golden_shoe_app

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_layout)

        var et_username = findViewById<EditText>(R.id.et_username) as EditText
        var et_password = findViewById<EditText>(R.id.et_password) as EditText
        var b_submit = findViewById<Button>(R.id.b_submit) as Button
        var b_register = findViewById<Button>(R.id.b_register) as Button

        b_submit.setOnClickListener {
            val username = et_username.text
            val password =  et_password.text
            Toast.makeText(this@LoginActivity, username, Toast.LENGTH_LONG).show()

            val payload = JSONObject()
            payload.put("username",username)
            payload.put("password",password)

            val url = "http://10.0.2.2:8000/api/p/auth/login/"
            val accountReq = JsonObjectRequest(Request.Method.POST, url, payload, Response.Listener<JSONObject>{
                val FILE_KEY = "GoldenShoe"
                val sharedPref = this.getSharedPreferences(FILE_KEY, Context.MODE_PRIVATE)
                val KEY_TOKEN = "Token"
                with(sharedPref.edit()) {
                    putString(KEY_TOKEN, it.get("key").toString())
                    commit()
                }
                finish()

            }, Response.ErrorListener { Toast.makeText(this, "Unable to log in!", Toast.LENGTH_LONG).show() })
            RequestSingleton.getInstance(this.applicationContext).addToRequestQueue(accountReq)
        }
    }
}