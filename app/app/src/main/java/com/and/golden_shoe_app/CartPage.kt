package com.and.golden_shoe_app

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.cart_page.*
import org.json.JSONArray
import org.json.JSONObject


class CartPage : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.cart_page)

        val total = intent.getStringExtra("total")
        val products = intent.getStringExtra("products")

        val cartProducts = arrayListOf<Product>()
        val jsonArr = JSONArray(products)
        for(i in 0 until jsonArr.length()) {
            val jsonObj = jsonArr.getJSONObject(i)
            var url = "http://10.0.2.2:8000${jsonObj.get("image").toString()}"
            Log.d("DEBUG", url)
            cartProducts.add(Product(jsonObj.get("id").toString().toInt(),
                                      jsonObj.get("category_name").toString(),
                                      jsonObj.get("name").toString(),
                                      jsonObj.get("description").toString(),
                                      url,
                                      jsonObj.get("price").toString().toDouble(),
                                      jsonObj.get("stock").toString().toInt()))

        }

        cartTotal.text = "Total: £$total"
        val gridLayoutManager = GridLayoutManager(this, 1)
        cartRV.layoutManager = gridLayoutManager
        cartRV.adapter = RecyclerAdapter(cartProducts)

    }
}