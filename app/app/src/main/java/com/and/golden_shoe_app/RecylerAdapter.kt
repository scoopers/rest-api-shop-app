package com.and.golden_shoe_app

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class RecyclerAdapter(private val products: ArrayList<Product>) :
RecyclerView.Adapter<RecyclerAdapter.ProductHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ProductHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_item_row, parent, false)
        val holder = ProductHolder(view)
        view.setOnClickListener {

            val intent = Intent(parent.context, ProductPage::class.java)
            val product = products[holder.adapterPosition]
            intent.putExtra("name", product.name)
            intent.putExtra("id", product.id)
            intent.putExtra("description", product.description)
            intent.putExtra("category", product.category)
            intent.putExtra("url", product.photoUrl)
            intent.putExtra("price", product.price)
            intent.putExtra("stock", product.stock)
            parent.context.startActivity(intent)
        }
        return holder
    }

    override fun getItemCount() = products.size

    override fun onBindViewHolder(holder: RecyclerAdapter.ProductHolder, position: Int) {
        val product = products[position]
        Picasso.get().load(product.photoUrl).into(holder.image)
        holder.name.text = product.name
        holder.price.text = "£${product.price.toString()}"
    }

    class ProductHolder(v: View) : RecyclerView.ViewHolder(v) {
        val image: ImageView = v.findViewById(R.id.itemImage)
        val name: TextView = v.findViewById(R.id.itemName)
        val price: TextView = v.findViewById(R.id.itemPrice)
    }

}
