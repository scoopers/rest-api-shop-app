package com.and.golden_shoe_app

import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.account_page.*


class AccountPage : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.account_page)

        val username = intent.getStringExtra("username")
        val email = intent.getStringExtra("email")
        val fname = intent.getStringExtra("first_name")
        val lname = intent.getStringExtra("last_name")

        accUsername.text = "Username: ${username}"
        accEmail.text = "Email: ${email}"
        accFName.text = "First name: ${fname}"
        accLName.text = "Last name: ${lname}"

        accLogout.setOnClickListener {
            val url = "http://10.0.2.2:8000/api/p/auth/logout/"
            val logoutReq = StringRequest(Request.Method.POST, url, Response.Listener<String>{
                Toast.makeText(this, "Logged out!", Toast.LENGTH_LONG).show()
                val FILE_KEY = "GoldenShoe"
                val sharedPref = this.getSharedPreferences(FILE_KEY, Context.MODE_PRIVATE)
                val KEY_TOKEN = "Token"
                with(sharedPref.edit()) {
                    putString(KEY_TOKEN, "")
                    commit()
                }
                finish()
            }, Response.ErrorListener { Toast.makeText(this, "Unable to log out!", Toast.LENGTH_LONG).show() })
            RequestSingleton.getInstance(this).addToRequestQueue(logoutReq)
        }
    }
}