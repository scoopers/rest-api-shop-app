package com.and.golden_shoe_app

import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.product_page.*
import org.json.JSONObject

class ProductPage : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.product_page)

        val id = intent.getIntExtra("id", 0)
        val name = intent.getStringExtra("name")
        val category = intent.getStringExtra("category")
        val url = intent.getStringExtra("url")
        val price = intent.getDoubleExtra("price", 0.0)
        val stock = intent.getIntExtra("stock", 0)
        val description = "Description: ${intent.getStringExtra("description")}"

        pageName.text = name
        Picasso.get().load(url).into(pageImage)
        pagePrice.text = "£${price}"
        pageDescription.text = description
        pageStock.text = "Quantity: ${stock}"

        val FILE_KEY = "GoldenShoe"
        val KEY_TOKEN = "Token"
        val sharedPref = this.getSharedPreferences(FILE_KEY, Context.MODE_PRIVATE)

        pageAdd.setOnClickListener {
            val payload = JSONObject()
            payload.put("pid",id.toString())
            Log.d("DEBUG", payload.toString())
            val aurl = "http://10.0.2.2:8000/api/p/cart/add/"
            val accountReq = object: JsonObjectRequest(Request.Method.POST, aurl, payload, Response.Listener<JSONObject>{
                Toast.makeText(this, "Added to basket!", Toast.LENGTH_LONG).show()
            }, Response.ErrorListener { Toast.makeText(this, "Unable to process request!", Toast.LENGTH_LONG).show() })  {
                override fun getHeaders(): MutableMap<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Authorization"] = "Token  ${sharedPref.getString(KEY_TOKEN, "")}"
                    headers["Content-Type"] = "application/json; charset=utf-8"
                    return headers
                }
            }

            RequestSingleton.getInstance(this.applicationContext).addToRequestQueue(accountReq)
        }

        pageRemove.setOnClickListener {
            val payload = JSONObject()
            payload.put("pid",id.toString())
            val rurl = "http://10.0.2.2:8000/api/p/cart/remove/"
            val accountReq = object: JsonObjectRequest(Request.Method.POST, rurl, payload, Response.Listener<JSONObject>{
                Toast.makeText(this, "Removed from basket!", Toast.LENGTH_LONG).show()
            }, Response.ErrorListener { Toast.makeText(this, "Unable to process request!", Toast.LENGTH_LONG).show() })  {
                override fun getHeaders(): MutableMap<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Authorization"] = "Token  ${sharedPref.getString(KEY_TOKEN, "")}"
                    headers["Content-Type"] = "application/json; charset=utf-8"
                    return headers
                }
            }

            RequestSingleton.getInstance(this.applicationContext).addToRequestQueue(accountReq)
        }

    }
}